const currentURL = window.location.href;
let accessToken;
let expiresIn;
let check_accessToken = currentURL.match(/access_token=([^&]*)/);
let check_expiresIn = currentURL.match(/expires_in=([^&]*)/);

const clientId = 'b3a63e182217404abf3b10b24e984caa';
//const redirectURI = 'http://localhost:3000/';
const redirectURI = 'http://odotjdot.surge.sh';


export const Spotify = {
    
    
    getAccessToken() {
        
        if(accessToken) {
            return new Promise( resolve => resolve(accessToken));
        } else if(check_accessToken && check_expiresIn) {
            
            accessToken = check_accessToken[0].replace('access_token=', '');
            expiresIn = check_expiresIn;
            
            window.setTimeout(() => accessToken = '', expiresIn * 1000);
window.history.pushState('Access Token', null, '/');
            return accessToken;
            
        } else {
            
            window.location.replace(`https://accounts.spotify.com/authorize?client_id=${clientId}&redirect_uri=${redirectURI}&scope=user-read-private%20user-read-email%20playlist-modify-private%20playlist-modify-public%20playlist-read-private&response_type=token&state=1988676723`);

        }

    }, // end getAccessToken() 
    
    
    search(term) {
        
        Spotify.getAccessToken();
        let tracksArr = [];
        return fetch(`https://api.spotify.com/v1/search?type=track&q=${term}`, {
            headers: {Authorization: `Bearer ${accessToken}`}
        }).then( response => {
            if(response.ok) {
                return response.json();
            }
            throw new Error('Request failed!');
            
            }, networkError => console.log(networkError.message))
            .then( jsonResponse => {
            
            if(jsonResponse.tracks.items.length) {
                return jsonResponse.tracks.items.map( track => {
                       return {
                            id: track.id,
                            name: track.name,
                            artist: track.artists[0].name,
                            album: track.album.name,
                            uri: track.uri,
                        }
                 });

                } else {
                    return tracksArr;
                }
                
        });
        
    }, // end search()
    
    savePlaylist( playListName, playListTracks) {
        
        let userToken = Spotify.getAccessToken();
        let headersVar = {Authorization: `Bearer ${userToken}`};
        let headersPostVar = headersVar;
        headersPostVar["Content-type"] = 'application/json';
        let userID;
        let playlistID;
        
        // get user id
        return fetch('https://api.spotify.com/v1/me', {headers: headersVar})
            .then( response => { 
            if( response.ok ) {
                return response.json();
            } 
                throw new Error('Request failed!'); }, networkError => console.log(networkError.message)
            )
            .then( jsonResponse => {
                
            userID = jsonResponse.id;
            
            
            // name playlist and get id
            return fetch(`https://api.spotify.com/v1/users/${userID}/playlists/`, 
              {
                headers: headersPostVar,
                method: 'POST',
                body: JSON.stringify({ name: playListName })
              }
             ).then( response => { 
            if( response.ok ) {
                return response.json()
            } 
//                console.log(response);
                throw new Error('Request failed!'); 
            }, networkError => console.log(networkError.message))
            .then( jsonResponse => {
            
                playlistID = jsonResponse.id;
                
                // name send tracks to playlist
            return fetch(`https://api.spotify.com/v1/users/${userID}/playlists/${playlistID}/tracks`, 
                  {
                    headers: headersPostVar,
                    method: 'POST',
                    body: JSON.stringify({ uris: playListTracks })
                  }
                 ).then( response => { 
                if( response.ok ) {
                    return response.json()
                } 
                throw new Error('Request failed!'); }, networkError => console.log(networkError.message))
                .then( jsonResponse => {

                    console.log(jsonResponse);
                
                return jsonResponse;

            })
        })
            
            
            });

        
    }, // end savePlaylist()
    
}


