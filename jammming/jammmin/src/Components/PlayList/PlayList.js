import React from 'react';
import { TrackList } from '../TrackList/TrackList';


import './PlayList.css';


export class PlayList extends React.Component {
    constructor(props) {
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.state = { isRemoval: true }
    }
    
    handleNameChange(e) {
        
        this.props.onNameChange(e.target.value);

    }
    render() {
        return(
            <div className="Playlist">
              <input id="playlistNameInput" onChange={this.handleNameChange} defaultValue={'New Playlist'}/>
              <TrackList tracks={this.props.playlistTracks} onRemove={this.props.onRemove} isRemoval={this.state.isRemoval} />
              <a className="Playlist-save" onClick={this.props.onSave} >SAVE TO SPOTIFY</a>
            </div>        
        );
        
    }
}