import React from 'react';
import './SearchBar.css';

export class SearchBar extends React.Component {
    
    constructor(props) {
        super(props);
        this.search = this.search.bind(this);
        this.handleTermChange = this.handleTermChange.bind(this);
        this.state = { searchTerm: '' };
    }
    
    search(term) {
        term = this.state.searchTerm;
        this.props.onSearch(term);
    }
    
    handleTermChange(e) {
        
        let newTerm = e.target.value
        this.setState({ searchTerm: newTerm });
    }
    
    render(){
        return(
            <div className="SearchBar">
              <input id="searchbarInput" placeholder="Enter A Song, Album, or Artist" onChange={this.handleTermChange} />
              <a onClick={this.search}>SEARCH</a>
            </div>        
        
        );
    }
}