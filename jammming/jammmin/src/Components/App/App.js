import React from 'react';
import { SearchBar } from '../SearchBar/SearchBar';
import SearchResults from '../SearchResults/SearchResults';
import { PlayList } from '../PlayList/PlayList';
import { Spotify } from '../../util/Spotify'

import './App.css';

export class App extends React.Component {
    
    constructor(props) {
        super(props);
        this.addTrack = this.addTrack.bind(this);
        this.removeTrack = this.removeTrack.bind(this);
        this.updatePlaylistName = this.updatePlaylistName.bind(this);
        this.savePlaylist = this.savePlaylist.bind(this);
        this.search = this.search.bind(this);
        this.state = { searchResults: [],
              playlistName: 'Playlist name',
              playlistTracks: [],
             }
        
    }
    
    addTrack(track) {
        let is_added;
        for(let i = 0; i < this.state.playlistTracks.length; i++ ){
        
            if(track.id === this.state.playlistTracks[i].id) {
                    is_added = true;
            } else {
                
                is_added = false;
            }
        }
        
        if (!is_added) {
            let newPlaylist = this.state.playlistTracks.concat(track);
            this.setState({ playlistTracks: newPlaylist });
        } else {
            console.log('its there already, mane')
        }
    
        
    } // end addTrack
    
    removeTrack(track) {

        let remove_me = this.state.playlistTracks.indexOf(track);
        this.state.playlistTracks.splice(remove_me, 1);
        this.setState({ playlistTracks: this.state.playlistTracks });

    } // end removeTrack
    
    updatePlaylistName(name) {
        this.setState({ playlistName: name });
    } // end updatePlaylistName
    
    savePlaylist() {
        
        let trackURIs = [];
        let currentPlaylist = this.state.playlistTracks;
        let currentPlaylistName = this.state.playlistName;
        
        trackURIs = currentPlaylist.map( track => (
            track.uri
        ));
        let uriObj = {uris: trackURIs}
        console.log(uriObj);
        return Spotify.savePlaylist(currentPlaylistName, trackURIs).then( response => {
            
            this.setState({
                searchResults: [],
                playlistName: 'New Playlist Reset',
                playlistTracks: [],
            });
            
            document.getElementById('playlistNameInput').value = 'New Playlist';
            document.getElementById('searchbarInput').value = '';
                
        });
        
        
        

    } // end savePlaylist()
    
    search(term) {
        return Spotify.search(term).then( response => {
            this.setState({ searchResults: response });
        });

    } // end search()
    

    render() {
        return (
        <div>
          <h1>Ja<span className="highlight">mmm</span>ing</h1>
          <div className="App">
            <SearchBar onSearch={this.search} />
            <div className="App-playlist">
              <SearchResults SearchResults={this.state.searchResults} onAdd={this.addTrack} />
              <PlayList playlistTracks={this.state.playlistTracks} playlistName={this.state.playlistName} onRemove={this.removeTrack} onNameChange={this.updatePlaylistName} onSave={this.savePlaylist} />
            </div>
          </div>
        </div>
            );
      }
}

